#!/usr/bin/env python2.7
import sys
import subprocess
import os
import argparse
import random
import string

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


def run_fastx(cmds_list):
    with open('run_fastx.sh', 'w') as f:
        for line in cmds_list:
            f.write(line)
    subprocess.call(['bash', 'run_fastx.sh'])


def parse_fastx_output(f, region):
    block1 = False
    alignment_list = []
    stop_codon_alignment_list = []
    at_region = False
    seen_start = False
    start = 0
    for line in f:
        if line.startswith('>>'):
            block1 = True
            continue
        if block1 is True:
            if line.startswith('>--'):
                break
            else:
                if line.startswith('; al_start') and not seen_start:
                    seen_start = True
                    start = int(line.split(' ')[2])
                if line.startswith(':') or line.startswith('.'):
                    alignment_list.append(line.rstrip('\n'))
                if line.startswith('>' + region):
                    at_region = True
                elif line[0].isalpha() and 'Function' not in line and at_region is True:
                    stop_codon_alignment_list.append(line.rstrip('\n'))
    alignment = ''.join(alignment_list)
    stop_codon_alignment = ''.join(stop_codon_alignment_list)
    alignment_length = float(len(alignment))
    if alignment_length == 0:
        return 'Error'
    if start > 1:
        return False
    alignment_stop = float(stop_codon_alignment.find('*'))
    alignment_frameshift = float(alignment.find('-'))
    if alignment_frameshift == -1 and alignment_stop != -1:
        interruption = alignment_stop
    elif alignment_frameshift != -1 and alignment_stop == -1:
        interruption = alignment_frameshift
    elif 0 <= alignment_stop < alignment_frameshift:
        interruption = alignment_stop
    elif alignment_frameshift >= 0:
        interruption = alignment_frameshift
    elif alignment_frameshift < 0 and alignment_stop < 0:
        return False
    else:
        return 'Error'
    alignment_affected = interruption / alignment_length
    if alignment_affected > 0.20:
        return True
    return False


def write_temp_fasta(sequence, header, output):
    SeqIO.write(SeqRecord(seq=Seq(sequence),
                          id=header,
                          description=''),
                output,
                'fasta')


def random_string():
    return ''.join([random.choice(string.ascii_letters) for i in range(10)])


def rm():
    for f in os.listdir(os.getcwd()):
        if f.endswith('.fasta') or f.endswith('.fastxout'):
            try:
                os.remove(f)
            except OSError:
                continue


def prepare_fastx_for_region(region, protein, ifasta, pfasta):
    nuc_name = random_string() + '.fasta'
    prot_name = random_string() + '.fasta'
    nuc_fasta = open(nuc_name, 'w')
    prot_fasta = open(prot_name, 'w')
    write_temp_fasta(sequence=ifasta[region],
                     header=region,
                     output=nuc_fasta)
    write_temp_fasta(sequence=pfasta[protein],
                     header=protein,
                     output=prot_fasta)
    out = random_string() + '.fastxout'
    fastx_cmd = ['./fastx.sh',
                  nuc_name,
                  prot_name,
                  out]
    return ' '.join(fastx_cmd) + '\n', out, [nuc_name, prot_name]


def iter_seqs(pseudos, intergenic_fasta, protein_fasta, pangenome_file):
    pseudogenes = set()
    fastx_script = []
    missed_fastxs = []
    fastx_outputs = {}
    region_fastas = {}
    pangenome_isolate_counts = get_pangenome_isolate_counts(pangenome_file)
    for intergenic_region, best_hit in pseudos.iteritems():
        cmd, outfile, fasta_names = prepare_fastx_for_region(region=intergenic_region,
                                                              protein=best_hit,
                                                              ifasta=intergenic_fasta,
                                                              pfasta=protein_fasta)
        fastx_script.append(cmd)
        fastx_outputs[intergenic_region] = outfile
        region_fastas[intergenic_region] = fasta_names
    run_fastx(cmds_list=fastx_script)
    for region, fastx in fastx_outputs.iteritems():
        with open(fastx, 'r') as f:
            pseudo = parse_fastx_output(f, region)
            if pseudo == 'Error':
                missed_fastxs.append(region)
                pseudo = False
        if pseudo and len(pangenome_isolate_counts[region]) / 99.0 < 0.75:
            pseudogenes.add(region)
    return pseudogenes, missed_fastxs


def pangenome_nuc_seqs(pangenome_nuc_fasta):
    seqs = {}
    for record in SeqIO.parse(pangenome_nuc_fasta, 'fasta'):
        seqs[record.id] = str(record.seq)
    return seqs


def pangenome_prot_seqs(pangenome_prot_fasta):
    seqs = {}
    for record in SeqIO.parse(pangenome_prot_fasta, 'fasta'):
        seqs[record.id] = str(record.seq)
    return seqs


def best_blast_hit(blast_lines):
    best_evalue = 1000
    best = ''
    for l in blast_lines:
        evalue = float(l[9])
        if evalue < best_evalue:
            best_evalue = evalue
            best = l
    return best


def iter_blastp_cds_pseudos(output):
    blastp = {}
    with open(output + '/blastp-pangenome-filtered.tsv', 'r') as f:
        for line in f:
            c = line.rstrip('\n').split('\t')
            try:
                blastp[c[0]] += [c[1:]]
            except KeyError:
                blastp[c[0]] = [c[1:]]
    for pseudogene, hits in blastp.iteritems():
        if len(hits) > 1:
            best_hit = best_blast_hit(hits)[0]
        else:
            best_hit = hits[0][0]
        blastp[pseudogene] = best_hit
    return blastp


def get_pangenome_isolate_counts(pangenome_file):
    isolate_counts = {}
    with open(pangenome_file, 'r') as f:
        for line in f:
            gene = line.split(':')[0]
            isolates = list(set(sorted([i.split('-L')[0] for i in line.rstrip('\n').split(' :')])))
            try:
                previous_isolates = isolate_counts[gene]
                isolate_counts[gene] += list(set(sorted(previous_isolates + isolates)))
            except KeyError:
                isolate_counts[gene] = isolates
    return isolate_counts


def arguments():
    parser = argparse.ArgumentParser(description='Runs fastx and parses output to identify CDS pseudogenes')
    parser.add_argument('-p', '--pangenome', help='Pangenome tsv file created by Roary')
    parser.add_argument('-n', '--pangenome_nuc', help='Pangenome nucleotide FASTA')
    parser.add_argument('-a', '--pangenome_prot', help='Pangenome protein FASTA')
    parser.add_argument('-o', '--output_dir', help='Output directory')
    return parser.parse_args()


def main():
    args = arguments()
    nucleotide_seqs = pangenome_nuc_seqs(args.pangenome_nuc)
    protein_seqs = pangenome_prot_seqs(args.pangenome_prot)
    putative_pseudos = iter_blastp_cds_pseudos(args.output_dir)
    true_pseudogenes, missed_pseudos = iter_seqs(pseudos=putative_pseudos,
                                                 intergenic_fasta=nucleotide_seqs,
                                                 protein_fasta=protein_seqs,
                                                 pangenome_file=args.pangenome)
    if missed_pseudos:
        with open(args.output_dir + '/missed-cds.pseudo.tsv', 'w') as f:
            for m in missed_pseudos:
                f.write('\t'.join([putative_pseudos[m], m]) + '\n')
    with open(args.output_dir + '/cds.truepseudos', 'w') as out:
        for region in list(true_pseudogenes):
            out.write(region + '\n')
    rm()


if __name__ == '__main__':
    main()
