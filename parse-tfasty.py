#!/usr/bin/env python2.7
import sys
import subprocess
import os
import random
import string

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


def parse_synteny(in_synteny_tsv):
    p = {}
    with open(in_synteny_tsv, 'r') as f:
        for line in f:
            c = line.rstrip('\n').split()
            p[c[1]] = c[0]
    return p


def load_fasta(fasta):
    seqs = {}
    for record in SeqIO.parse(fasta, 'fasta'):
        seqs[record.id] = str(record.seq)
    return seqs


def run_tfasty(cmds_list):
    with open('run_tfasty.sh', 'w') as f:
        for line in cmds_list:
            f.write(line)
    stdout = subprocess.call(['bash', 'run_tfasty.sh'], stdout=subprocess.PIPE)


def parse_tfasty_output(f, region):
    block1 = False
    alignment_list = []
    stop_codon_alignment_list = []
    at_region = False
    for line in f:
        if line.startswith('>>'):
            block1 = True
            continue
        if block1 is True:
            if line.startswith('>--'):
                break
            else:
                if line.startswith(':') or line.startswith('.'):
                    alignment_list.append(line.rstrip('\n'))
                if line.startswith('>' + region):
                    at_region = True
                elif line[0].isalpha() and 'Function' not in line and at_region is True:
                    stop_codon_alignment_list.append(line.rstrip('\n'))
    alignment = ''.join(alignment_list)
    stop_codon_alignment = ''.join(stop_codon_alignment_list)
    alignment_length = float(len(alignment))
    if alignment_length == 0:
        return 'Error', alignment_length
    alignment_stop = float(stop_codon_alignment.find('*'))
    alignment_frameshift = float(alignment.find('-'))
    if alignment_frameshift == -1 and alignment_stop != -1:
        interruption = alignment_stop
    elif alignment_frameshift != -1 and alignment_stop == -1:
        interruption = alignment_frameshift
    elif 0 <= alignment_stop < alignment_frameshift:
        interruption = alignment_stop
    elif alignment_frameshift >= 0:
        interruption = alignment_frameshift
    elif alignment_frameshift < 0 and alignment_stop < 0:
        return False, alignment_length
    else:
        return 'Error', alignment_length
    alignment_affected = interruption / alignment_length
    if alignment_affected > 0.20:
        return True, alignment_length
    return False, alignment_length


def write_temp_fasta(sequence, header, output):
    SeqIO.write(SeqRecord(seq=Seq(sequence),
                          id=header,
                          description=''),
                output,
                'fasta')


def random_string():
    return ''.join([random.choice(string.ascii_letters) for i in range(10)])


def rm():
    for f in os.listdir(os.getcwd()):
        if f.endswith('.fasta') or f.endswith('.tfastyout'):
            try:
                os.remove(f)
            except OSError:
                continue


def prepare_tfasty_for_region(region, protein, ifasta, pfasta):
    nuc_name = random_string() + '.fasta'
    prot_name = random_string() + '.fasta'
    nuc_fasta = open(nuc_name, 'w')
    prot_fasta = open(prot_name, 'w')
    write_temp_fasta(sequence=ifasta[region],
                     header=region,
                     output=nuc_fasta)
    write_temp_fasta(sequence=pfasta[protein],
                     header=protein,
                     output=prot_fasta)
    out = random_string() + '.tfastyout'
    tfasty_cmd = ['./tfasty.sh',
                  nuc_name,
                  prot_name,
                  out]
    return ' '.join(tfasty_cmd) + '\n', out, [nuc_name, prot_name]


def iter_seqs(pseudos, intergenic_fasta, protein_fasta):
    pseudogenes = []
    tfasty_script = []
    missed_tfastys = []
    tfasty_outputs = {}
    region_fastas = {}
    for intergenic_region, best_hit in pseudos.iteritems():
        cmd, outfile, fasta_names = prepare_tfasty_for_region(region=intergenic_region,
                                                              protein=best_hit,
                                                              ifasta=intergenic_fasta,
                                                              pfasta=protein_fasta)
        tfasty_script.append(cmd)
        tfasty_outputs[intergenic_region] = outfile
        region_fastas[intergenic_region] = fasta_names
    run_tfasty(cmds_list=tfasty_script)

    for region, tfasty in tfasty_outputs.iteritems():
        with open(tfasty, 'r') as f:
            pseudo, length = parse_tfasty_output(f, region)
            if pseudo == 'Error':
                missed_tfastys.append(region)
                pseudo = False
        if pseudo:
            pseudogenes.append(region + ';' + str(length * 3))
    return pseudogenes, missed_tfastys


def main():
    synteny_tsv = sys.argv[1]
    isolate = synteny_tsv.split('/')[-1].split('.')[0]
    intergenic_fasta = load_fasta('intergenic-fastas/' + isolate + '.fasta')
    protein_fasta = load_fasta('syntenic-regions/' + isolate + '.blastx.out.fasta')
    syntenic_pseudos = parse_synteny(synteny_tsv)
    true_pseudogenes, missed_pseudos = iter_seqs(pseudos=syntenic_pseudos,
                                                 intergenic_fasta=intergenic_fasta,
                                                 protein_fasta=protein_fasta)
    if missed_pseudos:
        with open('syntenic-regions/' + isolate + '.missed.pseudo.tsv', 'w') as f:
            for m in missed_pseudos:
                f.write('\t'.join([syntenic_pseudos[m], m]) + '\n')
    if synteny_tsv.endswith('.missed.pseudo.tsv'):
        mode = 'a'
    else:
        mode = 'w'
    with open('pseudogenes/' + isolate + '.truepseudos', mode) as out:
        for region in true_pseudogenes:
            out.write(region + '\n')
    rm()


if __name__ == '__main__':
    main()
