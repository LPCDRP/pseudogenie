
isolates ?= 
blast_db ?= 
out_dir ?= .
pangenome_file ?= 
pangenome_nuc ?=
pangenome_prot ?=
annotations ?= 

nproc ?= 4


fasta_loc := $(out_dir)/intergenic-fastas

all: intergenic cds

################
cds: $(out_dir)/pseudogenes/cds.truepseudos

$(out_dir)/cds.truepseudos: $(out_dir)/blastp-pangenome-filtered.tsv
	./parse-fastx.py -p $(pangenome_file) -n $(pangenome_nuc) -a $(pangenome_prot) -o $(out_dir)

$(out_dir)/blastp-pangenome-filtered.tsv: $(out_dir)/blastp-pangenome.tsv
	awk -F'\t' '{if($3 >= 50 && $4/$5 >= 0.1 && $4/$5 <= 0.8 && $11 <= 10e-6 && $5 <= $6){print $0}}' $< > $@

$(out_dir)/blastp-pangenome.tsv: $(GROUPHOME)/data/pangenome/runs/all/pan_genome-protein.fasta
	blastp -query $< -db blastdb/pangenome -num_threads $(nproc) -outfmt "6 qseqid sseqid pident length qlen slen qstart qend sstart send evalue bitscore" -out $@

###############
intergenic: $(foreach isolate, $(isolates), syntenic-regions/$(isolate).pseudo.tsv)

$(out_dir)/pseudogenes/%.truepseudos: $(out_dir)/syntenic-regions/%.pseudo.tsv pseudogenes/
	./parse-tfasty.py

$(out_dir)/syntenic-regions/%.pseudo.tsv: $(out_dir)/blastx-output/%.blastxout.filtered $(out_dir)/syntenic-regions/
	./pseudoGenie_synteny.py -d $< -o syntenic-regions/ -p $(pangenome_file) -a $(annotations)

$(out_dir)/blastx-output/%.blastxout.filtered: $(out_dir)/blastx-output/%.blastxout
	cat $< | ./filter-blastx.py > $@

$(out_dir)/blastx-output/%.blastxout: $(fasta_loc)/%.fasta $(out_dir)/blastx-output/
	blastx -query $< -db $(blast_db) -num_threads $(nproc) -outfmt "6 qseqid sseqid pident length qlen slen qstart qend sstart send evalue bitscore" -out $@


$(fasta_loc)/%.fasta: get_interregions.py $(fasta_loc)/
	./get_interregions.py -i $* -o $@

### Generic rules
%/:
	mkdir -p $@

.DELETE_ON_ERROR:
.ONESHELL:

