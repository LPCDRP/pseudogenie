#!/usr/bin/env python2.7
import sys
from math import ceil


def get_lowest_evalue(blastx_info):
    lowest_evalue = 50.0
    best_hit = ''
    for i in blastx_info:
        if float(i[9]) < lowest_evalue:
            best_hit = i
            lowest_evalue = float(i[9])
    return best_hit


def parse_blastx(blastx_file):
    gene_hits = {}
    for line in blastx_file:
        c = line.rstrip('\n').split('\t')
        qcov = float(c[3]) / ceil(float(c[4]) / 3.0)
        pid = float(c[2])
        evalue = float(c[10])
        if pid >= 50 and evalue < 10e-6 and qcov >= 0.50:
            try:
                gene_hits[c[0]] += [c[1:]]
            except KeyError:
                gene_hits[c[0]] = [c[1:]]

    for intergene, blastx in gene_hits.iteritems():
        best = get_lowest_evalue(blastx)
        print '\t'.join([intergene] + best)


def main():
    f = sys.stdin
    parse_blastx(blastx_file=f)


if __name__ == '__main__':
    main()
