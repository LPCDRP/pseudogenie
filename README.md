# pseudogenie

PseudoGenie: Pseudogene finder in pangenomes

### System Requirements
* Linux OS required
* Tested on Debian 8 (jessie)
* Install time 0 minutes

### Dependencies
* [FAST3](http://www.people.virginia.edu/~wrp/pearson.html)
* [python2.7](https://www.python.org/downloads/release/python-2715/)
* [Biopython](https://biopython.org/wiki/Download)
* [NCBI-BLAST](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/)

### How to install
All required executables are available

### How to run
```
make -f pseudogenie.mk 
    isolates=isolates.txt
    blast_db=pangenome
    out_dir=./
    pangenome_file=pan_genome.tsv
    pangenome_nuc=pan_genome_nucleotide.fasta
    pangenome_prot=pan_genome_protein.fasta
    annotations=annotations/
    nproc=2
```
