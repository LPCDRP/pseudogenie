#!/usr/bin/env python
import re
import os
import subprocess
import argparse

script_dir = os.path.abspath(os.path.dirname(__file__))
rv = {}
g2g = {}

with open('/'.join(script_dir.split('/')[0:-1]) + '/data_files/H37Rv-Tuberculist-annotations.tsv', 'r') as check:
    for line in check:
        column = line.rstrip("\n").split("\t")
        g2g[column[1]] = column[0]
        rv[column[0]] = column[1]


def grep_ltags(gene, gffs, annotation_dir):
    grep_genes2 = []
    for gff in gffs:
        isolate= gff.split('-L')[0]
        grep_gene_out = subprocess.Popen(['grep -w -C 1 ' + gene + " " +
                                          annotation_dir + isolate + ".gff"],
                                         shell=True, stdout=subprocess.PIPE)
        x = ([i for i in grep_gene_out.stdout])
        grep_genes = re.findall(r"gene=(.*?);", str(x))
        for i in grep_genes:
            try:
                grep_genes2.append(g2g[i])
            except KeyError:
                grep_genes2.append(i)
    return grep_genes2


def grep_cds_coords(iso,in_gene, annotation_dir):
    out = {}
    try:
        in_gene2 = rv[in_gene]
    except KeyError:
        in_gene2 = in_gene

    grep_gene_out = subprocess.Popen(['grep gene='+ in_gene2 + " " +
                                      annotation_dir + iso + ".gff"],
                                     shell=True, stdout=subprocess.PIPE)
    x = ([i for i in grep_gene_out.stdout])
    grep_cds = ([i.split("CDS\t")[-1].split("\t.")[0].split("\t") for i in x])
    try:
        out[in_gene].append(grep_cds)
    except KeyError:
        out[in_gene] = grep_cds
    return out


def grep_protein(in_gene, annotation_dir):
    try:
        in_gene = rv[in_gene]
    except KeyError:
        in_gene = in_gene
    grep_gene_out = subprocess.Popen(['grep gene='+ in_gene + " " +
                                      annotation_dir + str("*") + ".gff"],
                                     shell=True, stdout=subprocess.PIPE)
    x = ([i for i in grep_gene_out.stdout])
    grep_genes = re.findall(r"translation=(.*?);", str(x))
    if len(grep_genes) < 1:
        try:
            in_gene = g2g[in_gene]
        except KeyError:
            in_gene = in_gene
        grep_gene_out = subprocess.Popen(['grep gene='+ in_gene + " " +
                                          annotation_dir + str("*") + ".gff"],
                                         shell=True, stdout=subprocess.PIPE)
        x = ([i for i in grep_gene_out.stdout])
        grep_genes = re.findall(r"translation=(.*?);", str(x))
    return grep_genes[0]



def load_pangenome(pangenome_file):
    pangenome_dict = {}
    with open(pangenome_file, 'r') as f:
        for line in f:
            try:
                pangenome_dict[line.split(': ')[0]] += line.split(': ')[1].split('\t')
            except KeyError:
                pangenome_dict[line.split(': ')[0]] = line.split(': ')[1].split('\t')
    return pangenome_dict


def grep_all(in_gene, annotation_dir):
    in_gene = in_gene[0]
    grep_gene_out = subprocess.Popen(['grep -C 1 ='+ in_gene + " " +
                                      annotation_dir + str("*") + ".gff"],
                                     shell=True, stdout=subprocess.PIPE)
    x = ([i for i in grep_gene_out.stdout])
    grep_genes = re.findall(r"gene=(.*?);", str(x))
    grep_genes2 = []
    for i in grep_genes:
        try:
            grep_genes2.append(g2g[i])
        except KeyError:
            grep_genes2.append(i)

    return grep_genes2


def parse_pseudo(blastx_file, annotations, pangenome):
    iso_protein = {}
    true_pseudos = {}
    name = str(blastx_file).split(".blastxout.filtered")[0].split("/")[-1]
    gene_check_synteny = []
    pangenome = load_pangenome(pangenome)
    with open (blastx_file, 'r') as fin:
        for line in fin:
            line = line.rstrip("\n")
            e = line
            gene = e.split("\t")[1]
            try:
                protein = grep_protein(gene, annotations)
            except IndexError:
                continue
            try:
                iso_protein[name].append([gene, protein])
            except KeyError:
                iso_protein[name] = [[gene,protein]]
            description = e.split("\t")[0]
            synteny = e.split("\t")[0].split(";")[0].split(":")
            evalue = e.split("\t")[10]
            pseudo_coords = [e.split("\t")[6], e.split("\t")[7]]
            gene_check_synteny.append([gene, synteny, evalue, pseudo_coords, description])
    for blastx_out_line in gene_check_synteny:
        gene = blastx_out_line[0]
        try:
            surrounding_genes = grep_ltags(gene, gffs=pangenome[gene])
        except KeyError:
            surrounding_genes = grep_all(gene, annotations)
        intergenic_genes = blastx_out_line[0]
        qstart = int(blastx_out_line[3][0])
        qend = int(blastx_out_line[3][1])
        try:
            surrounding_leftgene_intergenic = g2g[blastx_out_line[1][0]]
        except KeyError:
            surrounding_leftgene_intergenic = blastx_out_line[1][0]
        try:
            surrounding_rightgene_intergenic = g2g[blastx_out_line[1][1]]
        except KeyError:
            surrounding_rightgene_intergenic = blastx_out_line[1][1]

        if surrounding_leftgene_intergenic in surrounding_genes and \
                surrounding_rightgene_intergenic in surrounding_genes:
            if qstart < qend:
                try:
                    true_pseudos[name].append([intergenic_genes, blastx_out_line[4]])
                except KeyError:
                    true_pseudos[name] = [[intergenic_genes, blastx_out_line[4]]]
            if qstart > qend:
                try:
                    true_pseudos[name].append([intergenic_genes, blastx_out_line[4]])
                except KeyError:
                    true_pseudos[name] = [[intergenic_genes, blastx_out_line[4]]]
    return true_pseudos, iso_protein


def arguments():
    parser = argparse.ArgumentParser(description='Greps synteny for all putative pseudogenes.')

    parser.add_argument('-d', '--dir', help='Path to "blastx.filtered" isolate file.', dest="dir",
                        required=True)
    parser.add_argument('-o', '--out_dir', help='Output directory')
    parser.add_argument('-p', '--pangenome', help='Pangenome file created by Roary')
    parser.add_argument('-a', '--annotations', help='GFF annotations')
    return parser.parse_args()


def main():
    args = arguments()
    blastx_output= args.dir

    true_pseudos,iso_protein = parse_pseudo(blastx_output, annotations=args.annotations, pangenome=args.pangenome)
    for iso, pseudo in true_pseudos.items():
        with open(args.out_dir + '/' + iso + ".pseudo.tsv", 'w') as fout:
            for pseudos in pseudo:
                fout.write(pseudos[0]+"\t"+pseudos[1]+"\n")
    for iso, protein in iso_protein.items():
        with open(args.out_dir + '/' + iso+".blastx.out.fasta",'w') as fout2:
            for prot in protein:
                fout2.write(">"+prot[0]+"\n"+prot[1] +'\n')


if __name__ == '__main__':
    main()
