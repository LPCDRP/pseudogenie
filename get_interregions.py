#!/usr/bin/env python2.7

__author__ = "Deepika Gunasekaran, James N O'Neill"
__version__ = "0.0.5"
__maintainer__ = "James N O'Neill"
__email__ = "jnoneill@ucdavis.edu"
__status__ = "Development"


import sys
import argparse
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import collections
rv = {}
g = {}

with open("/grp/valafar/resources/mtb-reconstruction/genes.tsv", 'r') as check:
    for line in check:
        column = line.rstrip("\n").split("\t")
        g[column[1]] = column[0]
        rv[column[0]] = column[1]

def get_ordered_features(feature_list):
    """
    :param feature_list: list of features of type SeqFeature
    :return: sorted list of features, sorted by genomic location
    """
    features_dict = {}
    ordered_features = []
    for feature in feature_list:
        feature_start = int(feature.location.start)
        if feature_start not in features_dict.keys():
            features_dict[feature_start] = [feature]
        else:
            features_dict[feature_start].append(feature)
    ordered_features_dict = collections.OrderedDict(sorted(features_dict.items()))
    for position in ordered_features_dict.keys():
        features_in_position = ordered_features_dict[position]
        for feature in features_in_position:
            ordered_features.append(feature)
    return ordered_features


#################################################################################
# Copyright(C) 2009 Iddo Friedberg & Ian MC Fleming
# Released under Biopython license. http://www.biopython.org/DIST/LICENSE
# Do not remove this comment
# This function was modified by Deepika Gunasekaran
def get_interregions(embl_record, isolate, intergene_length=1):
    seq_record = embl_record
    cds_list = []
    intergenic_records = []
    intergenic_positions = []
    pre_gene = {}
    post_gene = {}
    # Loop over the genome file, get the CDS features on each of the strands
    for feature in seq_record.features:
        if feature.type != 'CDS' or feature.type != 'rRNA' or feature.type != 'tRNA':
            continue
        try:
            gene1 = g[feature.qualifiers['gene'][0]]
        except KeyError:
            gene1 = feature.qualifiers['gene'][0]
        start = feature.location.start.position
        end = feature.location.end.position
        strand = '+' if feature.strand == 1 else '-'
        cds_list.append((start, end, strand))
        pre_gene[end + 1] = gene1
        post_gene[start] = gene1
    for i, pospair in enumerate(cds_list[1:]):
        # Compare current start position to previous end position
        last_end = cds_list[i][1]
        this_start = pospair[0]
        if this_start - last_end >= intergene_length:
            intergene_seq = seq_record.seq[last_end:this_start]
            strand_string = pospair[2]
            intergenic_records.append(
                  SeqRecord(intergene_seq,id="%s-ign-%d" % (seq_record.name, i),
                            description="%s %d-%d %s" % (seq_record.name, last_end + 1,
                                                         this_start, strand_string)))
            intergenic_positions.append((last_end + 1, this_start, strand_string))
    return intergenic_records, intergenic_positions, pre_gene, post_gene
#####################################################################################


def main():
    # This script takes as options -i <isolate_id> -o <output_file> from the commandline.
    parser = argparse.ArgumentParser(description='Identifying intergenic regions',
                                     epilog='Isolate ID must be specified')
    parser.add_argument('-g', '--gbk', help='Genbank file. Must include tRNA and rRNA annotations!')
    parser.add_argument('-o', '--output', help='Output file name')
    args = parser.parse_args()
    if args.isolate is None:
        parser.print_help()
        sys.exit('Isolate ID must be specified')
    annomerge_fp = args.gbk
    try:
        isolate_seq = SeqIO.parse(annomerge_fp, 'genbank')

        isolate_records = list(isolate_seq)

    except IOError:
        sys.exit('Gene ID Does not Exist')
    contig_num = 1
    output_file = open(args.output, 'w+')
    for rec in isolate_records:
        ordered_rec = rec[:]
        ordered_rec.features = []
        isolate_contig_features = rec.features
        ordered_contig_features = get_ordered_features(isolate_contig_features)
        ordered_rec.features = ordered_contig_features
        records, positions, pre_gene, post_gene = get_interregions(ordered_rec, args.gbk.split('/')[-1].split('.')[0],
                                                                   intergene_length=1)

        for i, r in enumerate(records):
            ig = '>'+pre_gene[positions[i][0]]+":" + post_gene[positions[i][1]] + \
                 ";" + str(positions[i][0]) + \
                 ":" + str(positions[i][1]) + \
                 ";" + str(positions[i][2]) + \
                 ';' + str(len(records[i])) + \
                 '\n' + str(rec.seq)[int(positions[i][0]):int(positions[i][1]) + 1] + \
                 "\n"
            output_file.write(ig)
        contig_num += 1


if __name__ == "__main__":
    main()
