#!/bin/sh

intergenic=$1
protein=$2
output=$3

echo "tfasty36 -m 10 $protein $intergenic > $output"
tfasty36 -m 10 $protein $intergenic > $output
wait

